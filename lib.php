<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details.
 *
 * @package    report
 * @subpackage completiongrade
 * @copyright  2009 Sam Hemelryk
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

/**
 * This function extends the navigation with the report items
 *
 * @param navigation_node $navigation The navigation node to extend
 * @param stdClass $course The course to object for the report
 * @param stdClass $context The context of the course
 */
function report_completiongrade_extend_navigation_course($navigation, $course, $context) {
    global $CFG;

    require_once($CFG->libdir.'/completionlib.php');

    if (has_capability('report/completiongrade:view', $context)) {
        $completiongrade = new completion_info($course);
        if ($completiongrade->is_enabled() && $completiongrade->has_criteria()) {
            $url = new moodle_url('/report/completiongrade/index.php', array('course'=>$course->id));
            $navigation->add(get_string('pluginname','report_completiongrade'), $url, navigation_node::TYPE_SETTING, null, null, new pix_icon('i/report', ''));
        }
    }
}

/**
 * This function extends the course navigation with the report items
 *
 * @param navigation_node $navigation The navigation node to extend
 * @param stdClass $user
 * @param stdClass $course The course to object for the report
 */
function report_completiongrade_extend_navigation_user($navigation, $user, $course) {

    return; //TODO: this plugin was not linked from navigation in 2.0, let's keep it that way for now --skodak

    if (report_completiongrade_can_access_user_report($user, $course)) {
        $url = new moodle_url('/report/completiongrade/user.php', array('id'=>$user->id, 'course'=>$course->id));
        $navigation->add(get_string('coursecompletiongrade'), $url);
    }
}

/**
 * Is current user allowed to access this report
 *
 * @private defined in lib.php for performance reasons
 *
 * @param stdClass $user
 * @param stdClass $course
 * @return bool
 */
function report_completiongrade_can_access_user_report($user, $course) {
    global $USER, $CFG;

    if (empty($CFG->enablecompletiongrade)) {
        return false;
    }

    if ($course->id != SITEID and !$course->enablecompletiongrade) {
        return false;
    }

    $coursecontext = context_course::instance($course->id);
    $personalcontext = context_user::instance($user->id);

    if ($user->id == $USER->id) {
        if ($course->showreports and (is_viewing($coursecontext, $USER) or is_enrolled($coursecontext, $USER))) {
            return true;
        }
    } else if (has_capability('moodle/user:viewuseractivitiesreport', $personalcontext)) {
        if ($course->showreports and (is_viewing($coursecontext, $user) or is_enrolled($coursecontext, $user))) {
            return true;
        }

    }

    // Check if $USER shares group with $user (in case separated groups are enabled and 'moodle/site:accessallgroups' is disabled).
    if (!groups_user_groups_visible($course, $user->id)) {
        return false;
    }

    if (has_capability('report/completiongrade:view', $coursecontext)) {
        return true;
    }

    return false;
}

/**
 * Return a list of page types
 * @param string $pagetype current page type
 * @param stdClass $parentcontext Block's parent context
 * @param stdClass $currentcontext Current context of block
 * @return array
 */
function report_completiongrade_page_type_list($pagetype, $parentcontext, $currentcontext) {
    $array = array(
        '*'                       => get_string('page-x', 'pagetype'),
        'report-*'                => get_string('page-report-x', 'pagetype'),
        'report-completiongrade-*'     => get_string('page-report-completiongrade-x',  'report_completiongrade'),
        'report-completiongrade-index' => get_string('page-report-completiongrade-index',  'report_completiongrade'),
        'report-completiongrade-user'  => get_string('page-report-completiongrade-user',  'report_completiongrade')
    );
    return $array;
}
